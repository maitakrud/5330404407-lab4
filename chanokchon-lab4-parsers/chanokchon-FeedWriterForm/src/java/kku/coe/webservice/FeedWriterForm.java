package kku.coe.webservice;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.SAXException;

@WebServlet(name = "FeedWriterForm", urlPatterns = {"/FeedWriterForm"})
public class FeedWriterForm extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String top = request.getParameter("topic");
        String link = request.getParameter("url");
        String des = request.getParameter("description");
        ArrayList<String> topArrayList = new ArrayList<String>();
        ArrayList<String> linkArrayList = new ArrayList<String>();
        ArrayList<String> desArrayList = new ArrayList<String>();
        ArrayList<String> pubDateArrayList = new ArrayList<String>();
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            FeedWriterForm fw = new FeedWriterForm();
            addToArray(topArrayList, linkArrayList, desArrayList, pubDateArrayList, top, link, des);
            String feed = fw.createRssTree(topArrayList, linkArrayList, desArrayList, pubDateArrayList, doc);
            out.print(feed);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String createRssTree(ArrayList<String> topArrayList, ArrayList<String> linkArrayList, 
            ArrayList<String> desArrayList, ArrayList<String> pubdateArrayList, Document doc) throws Exception { //root element rss
        Element rss = doc.createElement("rss");
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);
        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleT);
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode(
                "Khon Kean University Information NewsRss Feed");
        desc.appendChild(descT);
        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);
        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);
        

        for (int i = 0; i < topArrayList.size(); i++){
        Element item = doc.createElement("item");
        channel.appendChild(item);
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(topArrayList.get(i));
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(desArrayList.get(i));
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(linkArrayList.get(i));
        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode(pubdateArrayList.get(i));
        pubDate.appendChild(pubDateT);
    }
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();
        String filename = "/Users/MaiTaKRud/NetBeansProjects/chanokchon-lab4-parsers/chanokchon-FeedWriterForm/web/feed.xml";
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public void addToArray(ArrayList<String> topArrayList, ArrayList<String> linkArrayList, ArrayList<String> desArrayList,
            ArrayList<String> pubDateArrayList, String top, String link, String des) throws ParserConfigurationException, SAXException, IOException {

        String feed = "/Users/MaiTaKRud/NetBeansProjects/chanokchon-lab4-parsers/chanokchon-FeedWriterForm/web/feed.xml";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document doc = parser.parse(feed);
        NodeList items = doc.getElementsByTagName("item");
        for (int i = 0; i < items.getLength(); i++) {
            Element item = (Element) items.item(i);
            String topText = getElemVal(item, "title");
            String desText = getElemVal(item, "description");
            String linkText = getElemVal(item, "link");
            String pubDateText = getElemVal(item, "pubDate");
            topArrayList.add(topText);
            desArrayList.add(desText);
            linkArrayList.add(linkText);
            pubDateArrayList.add(pubDateText);
        }
        topArrayList.add(top);
        desArrayList.add(des);
        linkArrayList.add(link);
        pubDateArrayList.add((new java.util.Date()).toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold> }

    protected String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}