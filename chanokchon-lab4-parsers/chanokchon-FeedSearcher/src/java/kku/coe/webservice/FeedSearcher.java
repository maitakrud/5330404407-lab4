package kku.coe.webservice;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.net.*;
import javassist.compiler.ast.Keyword;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "FeedSearcher", urlPatterns = {"/FeedSearcher"})
public class FeedSearcher extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("<html><body><table border='1'><tr><th>Title</th><th>Link</th></tr>");
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder parser = factory.newDocumentBuilder();
            String urlLoc = request.getParameter("url");
            String kw = request.getParameter("keyword");
            URL url = new URL(urlLoc);
            
            Document doc = parser.parse(url.openStream());
            NodeList items = doc.getElementsByTagName("item");
            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element) items.item(i);
                if(getElemVal(item, "title").toLowerCase().contains(kw.toLowerCase())){
                out.println("<tr><td>" + getElemVal(item, "title") + "</td>");
                String link = getElemVal(item, "link");
                out.println("<td><a href= '" + link + "'>" + link + "</a></td><tr>");
                }
            }
            out.print("</table></body></html>");
        } catch (Exception e) {
            System.out.print(e);
        } finally {
            out.close();
        }
    }
    
    protected String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}