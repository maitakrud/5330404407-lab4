
package chanokchon.xmlfilewriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;


public class ChanokchonXMLFileWriter {

   
    protected void processRequest(){
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            ChanokchonXMLFileWriter xmlFWriter = new ChanokchonXMLFileWriter();
            String feed = xmlFWriter.createXMLTree(doc);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public String createXMLTree(Document doc) throws IOException, TransformerConfigurationException, TransformerException {
        Element quotes = doc.createElement("quotes");
        doc.appendChild(quotes);
        
        Element quoteA = doc.createElement("quote");
        quotes.appendChild(quoteA);
        
        Element wordsA = doc.createElement("words");
        quoteA.appendChild(wordsA);
        
        Text wordstA = doc.createTextNode("Time is more value than money. "
                + "You can get more money, but you cannot get more time.");
        wordsA.appendChild(wordstA);
        
        Element byA = doc.createElement("by");
        quoteA.appendChild(byA);
        
        Text bytA = doc.createTextNode("Jim Rohn");
        byA.appendChild(bytA);
        
        Element quoteB = doc.createElement("quote");
        quotes.appendChild(quoteB);
        
        Element wordsB = doc.createElement("words");
        quoteB.appendChild(wordsB);
        
        Text wordstB = doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง"
                + " ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
        wordsB.appendChild(wordstB);
        
        Element byB = doc.createElement("by");
        quoteB.appendChild(byB);
        
        Text bytB = doc.createTextNode("ว. วชิรเมธี");
        byB.appendChild(bytB);
        
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();
        String filename = "/Users/MaiTaKRud/NetBeansProjects/chanokchon-lab4-parsers/chanokchon-XMLFileWriter/src"
                + "/chanokchon/xmlfilewriter/qoutes.xml";
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        
        return null;
    }
    
    public static void main(String[] args) {
        ChanokchonXMLFileWriter chXMLFW = new ChanokchonXMLFileWriter();
        chXMLFW.processRequest();
    }
}
