package chanokchon.xmlfilesearcher;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class ChanokchonXMLFileSearcher {

    public void searcher() throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {
        String keyword = "src/chanokchon/xmlfilesearcher/keyword.txt";
        String quotes = "src/chanokchon/xmlfilesearcher/quotes.xml";

        FileInputStream fisKey = null;
        BufferedReader readerKey = null;

        fisKey = new FileInputStream(keyword);
        readerKey = new BufferedReader(new InputStreamReader(fisKey));

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder parser = factory.newDocumentBuilder();
        Document doc = parser.parse(quotes);
        NodeList items = doc.getElementsByTagName("quote");


        String lineKey = readerKey.readLine();

        while (lineKey != null) {
            
            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element) items.item(i);
                if (getElemVal(item, "by").toLowerCase().contains(lineKey.toLowerCase())) {
                    System.out.println(getElemVal(item, "words") + "by"
                            +getElemVal(item, "by"));
                }
            }
            lineKey = readerKey.readLine();
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {
        ChanokchonXMLFileSearcher chXMLFS = new ChanokchonXMLFileSearcher();
        chXMLFS.searcher();
    }

    protected String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData cd = (CharacterData) child;
                return cd.getData();
            }
        } catch (Exception ex) {
            
        }
        return "";
    }
}
