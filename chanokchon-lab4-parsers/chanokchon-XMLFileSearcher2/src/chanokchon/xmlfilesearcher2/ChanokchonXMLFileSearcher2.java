package chanokchon.xmlfilesearcher2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class ChanokchonXMLFileSearcher2 {

    public void searcher() throws FileNotFoundException, IOException, XMLStreamException {

        XMLEventReader reader;
        boolean byFound = false;
        boolean itemFound = false;
        boolean wordsFound = false;
        String link = null;
        String eName;
        String words = null;
        FileInputStream fisKey = null;
        BufferedReader readerKey = null;

        String keyword = "src/chanokchon/xmlfilesearcher2/keyword.txt";
        String quotes = "src/chanokchon/xmlfilesearcher2/quotes.xml";

        fisKey = new FileInputStream(keyword);
        readerKey = new BufferedReader(new InputStreamReader(fisKey));

        String lineKey = readerKey.readLine();


        while (lineKey != null) {
            File u = new File(quotes);
            InputStream in = new FileInputStream(u);
            XMLInputFactory factory = XMLInputFactory.newInstance();
            reader = factory.createXMLEventReader(in);
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("quote")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("words")) {
                        wordsFound = true;
                    }
                    if (itemFound && eName.equals("by")) {
                        byFound = true;
                    }
                }
                if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    if (wordsFound) {
                        words = characters.getData();
                        wordsFound = false;
                    }
                    if (byFound) {
                        if (characters.getData().toLowerCase().contains(lineKey.toLowerCase())) {
                            System.out.println(words + " by " + characters.getData());
                        }
                    }
                }
            }
            lineKey = readerKey.readLine();
        }
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, XMLStreamException {
        ChanokchonXMLFileSearcher2 chXMLFS2 = new ChanokchonXMLFileSearcher2();
        chXMLFS2.searcher();
    }
}
