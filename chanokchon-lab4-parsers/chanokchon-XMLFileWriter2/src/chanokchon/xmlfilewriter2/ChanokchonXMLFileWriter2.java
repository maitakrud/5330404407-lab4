package chanokchon.xmlfilewriter2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;


public class ChanokchonXMLFileWriter2 {
    
    public void createXMLFileWriterDoc() throws Exception {
        String filename = "/Users/MaiTaKRud/NetBeansProjects/chanokchon-lab4-parsers/chanokchon-XMLFileWriter2/src"
                + "/chanokchon/xmlfilewriter2/qoutes.xml";
        File file = new File(filename);
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));
        xtw.writeStartDocument();
        xtw.writeStartElement("qoutes");
        xtw.writeStartElement("quote");
        xtw.writeStartElement("words");
        xtw.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time.");
        xtw.writeEndElement();
        xtw.writeStartElement("by");
        xtw.writeCharacters("Jim Rohn");
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeStartElement("quote");
        xtw.writeStartElement("words");
        xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
        xtw.writeEndElement();
        xtw.writeStartElement("by");
        xtw.writeCharacters("ว. วชิรเมธี");
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.writeEndElement();
        xtw.flush();
        xtw.close();
    }
    public static void main(String[] args) throws Exception {
        
        ChanokchonXMLFileWriter2 chXMLFW = new ChanokchonXMLFileWriter2();
        chXMLFW.createXMLFileWriterDoc();
    }
}
