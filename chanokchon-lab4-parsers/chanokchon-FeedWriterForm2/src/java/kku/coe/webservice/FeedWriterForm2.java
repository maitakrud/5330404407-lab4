package kku.coe.webservice;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import javax.xml.stream.*;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

@WebServlet(name = "FeedWriter2", urlPatterns = {"/FeedWriter2"})
public class FeedWriterForm2 extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        String top = request.getParameter("topic");
        String link = request.getParameter("url");
        String des = request.getParameter("description");
        ArrayList<String> topArrayList = new ArrayList<String>();
        ArrayList<String> linkArrayList = new ArrayList<String>();
        ArrayList<String> desArrayList = new ArrayList<String>();
        ArrayList<String> pubDateArrayList = new ArrayList<String>();

        try {
            addToArray(topArrayList, linkArrayList, desArrayList, pubDateArrayList, top, link, des);
            createRssDoc(topArrayList, linkArrayList, desArrayList, pubDateArrayList);
            out.println("<b><a href ='http://localhost:8080/chanokchon-FeedWriterForm2/feed.xml'>"
                    + "Rss Feed</a> was create successfully</b>");
        } catch (Exception e) {
            System.out.println();
        }
    }

    public void createRssDoc(ArrayList<String> topArrayList, ArrayList<String> linkArrayList,
            ArrayList<String> desArrayList, ArrayList<String> pubdateArrayList) throws Exception {
        String filename = "/Users/MaiTaKRud/NetBeansProjects/chanokchon-lab4-parsers/chanokchon-FeedWriterForm2/web/feed.xml";
        File file = new File(filename);
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));
        xtw.writeStartDocument("UTF-8", "1.0");
        xtw.writeStartElement("rs");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();
        
        for (int i = 0; i < topArrayList.size(); i++) {
            xtw.writeStartElement("item");
            xtw.writeStartElement("title");
            xtw.writeCharacters(topArrayList.get(i));
            xtw.writeEndElement();
            xtw.writeStartElement("description");
            xtw.writeCharacters(linkArrayList.get(i));
            xtw.writeEndElement();
            xtw.writeStartElement("link");
            xtw.writeCharacters(desArrayList.get(i));
            xtw.writeEndElement();
            xtw.writeStartElement("pubDate");
            xtw.writeCharacters(pubdateArrayList.get(i));
            xtw.writeEndElement();
            xtw.writeEndElement();
        }
        xtw.writeEndElement();
        xtw.flush();
        xtw.close();
    }

    public void addToArray(ArrayList<String> topArrayList, ArrayList<String> linkArrayList, ArrayList<String> desArrayList,
            ArrayList<String> pubDateArrayList, String top, String link, String des) throws FileNotFoundException, XMLStreamException {

        String eName;
        XMLEventReader reader;
        String quotes = "/Users/MaiTaKRud/NetBeansProjects/chanokchon-lab4-parsers/chanokchon-FeedWriterForm2/web/feed.xml";
        boolean itemFound = false;
        boolean topFound = false;
        boolean linkFound = false;
        boolean desFound = false;
        boolean dateFound = false;

        File u = new File(quotes);
        InputStream in = new FileInputStream(u);
        XMLInputFactory factory = XMLInputFactory.newInstance();
        reader = factory.createXMLEventReader(in);
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                StartElement element = (StartElement) event;
                eName = element.getName().getLocalPart();

                if (eName.equals("item")) {
                    itemFound = true;
                }
                if (itemFound && eName.equals("title")) {
                    topFound = true;
                }
                if (itemFound && eName.equals("description")) {
                    desFound = true;
                }
                if (itemFound && eName.equals("link")) {
                    linkFound = true;
                }
                if (itemFound && eName.equals("pubDate")) {
                    dateFound = true;
                }
            }
            if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    if (topFound) {
                        topArrayList.add(characters.getData());
                        topFound = false;
                    }
                    if (desFound) {
                        desArrayList.add(characters.getData());
                        desFound = false;
                    }
                    if (linkFound) {
                        linkArrayList.add(characters.getData());
                        linkFound = false;
                    }
                    if (dateFound) {
                        pubDateArrayList.add(characters.getData());
                        dateFound = false;
                    }
                }
        }
        
        topArrayList.add(top);
        desArrayList.add(des);
        linkArrayList.add(link);
        pubDateArrayList.add((new java.util.Date()).toString());
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}